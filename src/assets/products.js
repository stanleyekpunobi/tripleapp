var searchParameters = {};
var apiUrl = "https://api.apptriple.com/api/AffiliatePartner/GetAffiliateStoreProducts"
var shopifydata = Shopify;
if (document.readyState === 'complete')
  {
  var userid = document.getElementById("searchItems");
  var ref = getUrlParameter('ref');
    //console.log(ref);
    if (ref && ref.length > 0) {
      updateData('/cart/update.js', { note: ref })
      //jQuery.post('/cart/update.js', {note: ref});
    }
  if (userid === null) {

  }
  else {
    userid = document.getElementById("searchItems").getAttribute("data-userid");
    searchParameters = { Shop: Shopify.shop, UserId: userid };
    postData(apiUrl, searchParameters)
      .then(data => {
        document.getElementById("searchItems").style.display = "block";
        var productRow = document.querySelector("#searchItems");
        // list.innerHTML = products.map(function(newproduct) { return renderProduct(newproduct) }).join("");
        if(data.responseData.products.length === 0){

        }
        else{
          console.log(data.responseData.products);
          for (let i = 0; i < data.responseData.products.length; i++) {

            productRow.innerHTML += `<div class="product-box">
            <a target="_blank" id="${i}productboxlink" href="https://${data.responseData.products[i].shopDomain}/products/${data.responseData.products[i].handle}?ref=${data.responseData.products[i].affiliateTag}">
            <img id="${i}prodImg" src="${data.responseData.products[i].images[0].src}" alt="Product Image" width="90" height="110">
            <div class="product-title">
            <h3 id="${i}prodname">${data.responseData.products[i].title}</h3>
            </div>
          <p id="${i}prodprice" class="product-price">${data.responseData.products[i].variants[0].price} ${data.responseData.products[i].currency}
            </p>
            </a>
            <a id="action-button${i}" target="_blank" href="https://${data.responseData.products[i].shopDomain}/products/${data.responseData.products[i].handle}?ref=${data.responseData.products[i].affiliateTag}" class="action-button" style="border: black 1px solid;padding: 3px;font-size: 11px;border-radius: 4px;">Buy</a>
            </div>
         `
          }

          var actionButtons = document.querySelectorAll(".action-button");

          actionButtons.forEach(element => {
            if (data.responseData.settings?.buttonText != null) {
              //console.log(element);
              element.innerHTML = data.responseData.settings.buttonText;
            }
            else {
              element.innerHTML = "Buy";
            }
          });

          if (data.responseData.settings?.affiliateHeading != null) {
            document.getElementById("triple-heading").style.display = "block";
            document.getElementById("triple-heading").innerHTML = data.responseData.settings.affiliateHeading;
          }
          else {
            document.getElementById("triple-heading").style.display = "block";
            document.getElementById("triple-heading").innerHTML = "You may also like";
          }
        }

        document.getElementById("loader").style.display = "none";

      }).catch((error) => {
        console.log(error);
        document.getElementById("searchItems").style.display = "none";
        document.getElementById("loader").style.display = "none";

        document.getElementById("failedrequest").innerText = "No result found";
      });

  }
}


function getUrlParameter(sParam) {
  var sPageURL = decodeURIComponent(window.location.search.substring(1)),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined ? true : sParameterName[1];
    }
  }
};

async function postData(url = '', data = {}) {
  // Default options are marked with *
  const response = await fetch(url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'omit', // include, *same-origin, omit
    headers: {
      'content-type': 'application/json; charset=utf-8'
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    body: JSON.stringify(data) // body data type must match "Content-Type" header
  });
  return response.json(); // parses JSON response into native JavaScript objects
}

async function updateData(url = '', data = {}) {
  // Creating a XHR object
  let xhr = new XMLHttpRequest();
  // open a connection
  xhr.open("POST", url, true);
  // Set the request header i.e. which type of content you are sending
  xhr.setRequestHeader("Content-Type", "application/json");
  // Converting JSON data to string
  var data = JSON.stringify(data);
  // Sending data with the request
  xhr.send(data);
}

