import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { HandshakeModel } from "src/app/shared/models/handshake.model";
import { ResponseObject } from "src/app/shared/models/responseobject.model";
import { AuthService } from "src/app/shared/services/auth.service";

@Component({
  selector: "app-handshake",
  templateUrl: "./handshake.component.html",
  styleUrls: ["./handshake.component.css"],

})
export class HandshakeComponent implements OnInit {
  handShakeModel: HandshakeModel = new HandshakeModel();
  params: any;
  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {

    this.route.queryParams.subscribe((params) => {
      this.handShakeModel.Shop = params["shop"];
      this.handShakeModel.Hmac = params["hmac"];
      this.handShakeModel.Timestamp = +params["timestamp"];
      this.params = params;
    });
    this.authService.showloader.emit(true);
    this.authService.isAuthenticating.emit(true);
    this.authService
        .ShopifyHandshake(this.params)
        .subscribe((result: ResponseObject) => {
          console.log(result);
          if (result.isSuccessful) {
            this.authService.SaveToken(result);
            if (result.responseData.accessToken != undefined) {
              this.router.navigate([result.responseData.url]);
            } else {
              window.location.href = result.responseData.url;
            }
            //debugger;
            //this._location.go(result.responseData.url);
            this.authService.isAuthenticating.emit(false);
          }
          else {
            this.authService.showloader.emit(false);
          }
        });

    console.log(this.handShakeModel);
  }
}
