import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthModel } from '../shared/models/auth.model';
import { PlanModel } from '../shared/models/plan.model';
import { ResponseObject } from '../shared/models/responseobject.model';
import { AuthService } from '../shared/services/auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
  //providers:[AuthService]
})
export class AuthComponent implements OnInit {

  authModel: AuthModel = new AuthModel();

  plan: PlanModel = new PlanModel();

  constructor(private route: ActivatedRoute, private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.authModel.Shop = params['shop'];
      this.authModel.Hmac = params['hmac'];
      this.authModel.Timestamp = +params['timestamp'];
      this.authModel.Code = params['code'];
    });
   console.log(this.authModel);
   this.authService.isAuthenticating.emit(true);
    this.authService.showloader.emit(true);
    this.authService.AuthHandshake(this.authModel).subscribe((result: ResponseObject) => {
      console.log(result);
      if(result.isSuccessful){
        this.authService.SaveToken(result);
        this.authService.AuthCharge(result.responseData.accessToken, 1).subscribe((chargeresult: ResponseObject) => {
          console.log(chargeresult);
          if(chargeresult.isSuccessful){
            //debugger;
            window.location.href = chargeresult.responseData.url;
          }
        })
        this.authService.showloader.emit(false);
        this.authService.isAuthenticating.emit(false);

      }
      else{
          //this.router.navigate['error-page'];
          //show error modal
      }

    })
  }

}
