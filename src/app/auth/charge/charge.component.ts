import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ChargeModel } from "src/app/shared/models/charge.model";
import { ResponseObject } from "src/app/shared/models/responseobject.model";
import { AuthService } from "src/app/shared/services/auth.service";
import Swal, { SweetAlertResult } from "sweetalert2";

@Component({
  selector: "app-charge",
  templateUrl: "./charge.component.html",
  styleUrls: ["./charge.component.css"],
  providers:[AuthService]
})
export class ChargeComponent implements OnInit {
  chargeModel: ChargeModel = new ChargeModel();
  showChargeResult: boolean;
  constructor(
    private route: ActivatedRoute, private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
     this.chargeModel.ChargeId = params['charge_id'];
    });
    console.log(this.chargeModel);
    this.authService.showloader.emit(true);
    this.authService.AuthChargeResult(this.chargeModel).subscribe((result: ResponseObject) => {
      this.authService.isAuthenticating.emit(true);
      console.log(result);
      if(result.isSuccessful){
        //debugger;
        this.CreateAffiliateCharge();
        this.authService.isAuthenticating.emit(true);
        this.authService.showloader.emit(false);
      }
      else{
        if(result.responseData.url = 'charge-declined'){
          this.showChargeResult = true
          Swal.fire({
            icon: 'error',
            title: 'Plan Status',
            text: 'You have declined triple app charge on your store, you have a free plan which you can use to list up to five products on Triple',
            footer: '<a href>See how to upgrade your plan</a>',
            confirmButtonText:'Close'
          }).then((result:SweetAlertResult) => {
            window.location
          })
        }
        this.router.navigate([result.responseData.url]);
        this.authService.showloader.emit(false);
      }

    })
  }

  CreateAffiliateCharge(){
    console.log('charge affiliate')
    this.authService.showloader.emit(true);

    this.authService.AuthChargeAffiliate().subscribe((result:ResponseObject)=> {
      console.log(result);
      if(result.isSuccessful){
        //debugger;
        this.authService.isAuthenticating.emit(false);

        this.router.navigate([result.responseData.url]);
        //window.location.href = result.responseData.url;
      }
    })
  }
}
