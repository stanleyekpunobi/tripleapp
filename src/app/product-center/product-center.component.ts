import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Price } from '../shared/interface/price.interface';
import { product } from '../shared/models/product.interface';
import { ResponseObject } from '../shared/models/responseobject.model';
import { AuthService } from '../shared/services/auth.service';
import { ProductCenterService } from '../shared/services/productcenter.service';
import { ProductCenterDialogComponent } from './product-center-dialog/product-center-dialog.component';


@Component({
  selector: 'app-product-center',
  templateUrl: './product-center.component.html',
  styleUrls: ['./product-center.component.css'],
  providers:[ProductCenterService]
})

export class ProductCenterComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  breakpoint: number;
  pageEvent: PageEvent;
  pageSizeLength: number;
  pageSizeOptions: number[] = [12, 24, 48, 100];

  listedProducts: product[] = [];

  prices: Price[] = [
    {value: '1', viewValue: 'Low to High'},
    {value: '2', viewValue: 'High to Low'},
  ];

  commission: Price[] = [
    {value: '1', viewValue: '0% to 10%'},
    {value: '2', viewValue: '11% to 30%'},
    {value: '3', viewValue: '31% to 50%'},
    {value: '4', viewValue: '51% to 70%'},
    {value: '5', viewValue: '71% to 100%'},
    {value: '6', viewValue: 'Clear'},
  ];

  dataSource = new MatTableDataSource(this.listedProducts);

  public pricevalue: string;

  public commissionvalue: string;

  constructor(private productCenterService: ProductCenterService, public dialog: MatDialog, private authService: AuthService) { }

  ngOnInit(): void {
    this.authService.showloader.emit(true);
    this.productCenterService.GetListedProducts().subscribe((result: ResponseObject)=> {
      if(result.isSuccessful){
        this.listedProducts = result.responseData;

        //this.dataSource = new MatTableDataSource(this.listedProducts);
        this.pageSizeLength = this.listedProducts.length;
        const newProducts = this.listedProducts.slice(this.paginator.pageIndex * this.paginator.pageSize,
          (this.paginator.pageIndex * this.paginator.pageSize) + this.paginator.pageSize)
        this.dataSource = new MatTableDataSource(newProducts);
        console.log(this.listedProducts);
        this.authService.showloader.emit(false);
      }
    })

    this.productCenterService.isAdded.subscribe((result:any)=> {
      this.listedProducts[result.productindex].isAdded = result.isadded;
      console.log(result);
     });

    this.breakpoint = (window.innerWidth <= 415) ? 1 : 4;
  }

  onResize(event) {
    this.breakpoint = (event.target.innerWidth <= 415) ? 1 : 4;
  }

  OpenAddDialog(product: any) {
    let dialogRef = this.dialog.open(ProductCenterDialogComponent, {
      width:'300px',
      data: {product, isadding: true},
    });

    dialogRef.afterClosed().subscribe(result => {
      let product = this.listedProducts.findIndex(e => e.id == result);
      this.productCenterService.isAdded.emit({productindex:product, isadded:true });
    })
  }

  OpenRemoveDialog(product: any){
    let dialogRef = this.dialog.open(ProductCenterDialogComponent, {
      width:'300px',
      data: {product, isadding: false},
    });

    dialogRef.afterClosed().subscribe(result => {
      let product = this.listedProducts.findIndex(e => e.id == result);
      this.productCenterService.isAdded.emit({productindex:product, isadded:false });
    })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  GetData(event: PageEvent): PageEvent{
    console.log(event);
    const newProducts = this.listedProducts.slice(event.pageIndex * event.pageSize, (event.pageIndex * event.pageSize) + event.pageSize)
    this.dataSource = new MatTableDataSource(newProducts);
    //this.dataSource.paginator = this.paginator;
   return this.pageEvent;
  }

  handleCommissionChange(){
    if(this.commissionvalue == "1"){
      this.dataSource.filter = "0% 10%";
      const dataFiltered = this.listedProducts.filter(function(item){
        return item.commission >= 0 && item.commission <= 10.99
      })
      this.dataSource.filteredData =dataFiltered;
    }
    if(this.commissionvalue == "2"){
      const dataFiltered = this.listedProducts.filter(function(item){
        return item.commission >= 11 && item.commission <= 30.99
      })
      this.dataSource.filteredData =dataFiltered;
    }

    if(this.commissionvalue == "3"){
      const dataFiltered = this.listedProducts.filter(function(item){
        return item.commission >= 31 && item.commission <= 50.99
      })
      this.dataSource.filteredData =dataFiltered;
    }
    if(this.commissionvalue == "4"){
      const dataFiltered = this.listedProducts.filter(function(item){
        return item.commission >= 51 && item.commission <= 70.99
      })
      this.dataSource.filteredData =dataFiltered;
    }
    if(this.commissionvalue == "5"){
      const dataFiltered = this.listedProducts.filter(function(item){
        return item.commission >= 71 && item.commission <= 100
      })
      this.dataSource.filteredData =dataFiltered;
    }

    if(this.commissionvalue == "6"){
      this.dataSource = new MatTableDataSource(this.listedProducts);
    }


  }


}
