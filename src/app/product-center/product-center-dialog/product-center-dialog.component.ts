import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ResponseObject } from 'src/app/shared/models/responseobject.model';
import { AuthService } from 'src/app/shared/services/auth.service';
import { ProductCenterService } from 'src/app/shared/services/productcenter.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-product-center-dialog',
  templateUrl: './product-center-dialog.component.html',
  styleUrls: ['./product-center-dialog.component.css'],
  providers:[ProductCenterService]

})
export class ProductCenterDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public dialogData:any,public dialogRef: MatDialogRef<ProductCenterDialogComponent>,
  private snackBar: MatSnackBar,
  private productCenterService: ProductCenterService, private authService: AuthService) { }

  ngOnInit(): void {

  }
  AddAffiliateProduct() {
    console.log(this.dialogData.product);
    this.authService.showloader.emit(true);
    this.productCenterService.AddProduct(this.dialogData.product).subscribe((response:ResponseObject) => {
      if(response.isSuccessful){
        this.dialogRef.close(this.dialogData.product.id);
        Swal.fire({
          icon: 'success',
          title: 'Request Product',
          text: 'Product successfully requested',
          confirmButtonColor: '#4680FF'
        })

         //this.snackBar.open("Product successfully requested", "Request Product", {duration: 2000});
      }else{
        Swal.fire({
          icon: 'error',
          title: 'Request Product',
          text: response.message,
          confirmButtonColor: '#4680FF'

        })
      }
      this.authService.showloader.emit(false);
    })
  }

  RemoveAffiliateProduct(){
    console.log(this.dialogData.product);
    this.authService.showloader.emit(true);
    this.productCenterService.RemoveProduct(this.dialogData.product).subscribe((response:ResponseObject) => {
      if(response.isSuccessful){
        this.dialogRef.close(this.dialogData.product.id);
        Swal.fire({
          icon: 'success',
          title: 'Remove Product',
          text: 'Product successfully removed from affiliate list',
          confirmButtonColor: '#4680FF'
        })
        //  this.snackBar.open("Product successfully removed from affiliate list", "Remove Product", {duration: 2000});
      }else{
        Swal.fire({
          icon: 'error',
          title: 'Remove Product',
          text: response.message,
          confirmButtonColor: '#4680FF'

        })
      }
      this.authService.showloader.emit(false);

    })
  }
}
