import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductCenterDialogComponent } from './product-center-dialog.component';

describe('ProductCenterDialogComponent', () => {
  let component: ProductCenterDialogComponent;
  let fixture: ComponentFixture<ProductCenterDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductCenterDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductCenterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
