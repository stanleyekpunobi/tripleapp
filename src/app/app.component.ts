import { Component, OnInit } from '@angular/core';
import Swal, { SweetAlertResult } from 'sweetalert2';
import { AuthService } from './shared/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'ttriple';

  constructor(private authService: AuthService) {

  }
  ngOnInit(){
    // const authToken = this.authService.GetLocalStorageItem('authToken');
    // if(authToken == null){
    //   Swal.fire({
    //     icon: "error",
    //     title: "Invalid Session",
    //     text: "Your session is in valid",
    //     confirmButtonText: `Login`,
    //   }).then((result: SweetAlertResult) => {
    //     window.location.href = 'https://apptriple.com';
    //   });
    // }

  }
}
