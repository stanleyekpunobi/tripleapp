import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AffiliatePartnerComponent } from './affiliate-partner/affiliate-partner.component';
import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { ChargeComponent } from './auth/charge/charge.component';
import { HandshakeComponent } from './auth/handshake/handshake.component';
import { ErrorComponent } from './error/error.component';
import { ProductCenterComponent } from './product-center/product-center.component';
import { SettingsComponent } from './settings/settings.component';
import { StoreOwnerComponent } from './store-owner/store-owner.component';

const routes: Routes = [
  {path: '', component: ProductCenterComponent, pathMatch: 'full'},
  {path: 'handshake', component: HandshakeComponent },
  {path: 'auth', component: AuthComponent},
  {path: 'charge', component: ChargeComponent },
  {path: 'charge:id', component: ChargeComponent },
  {path: 'store-owner', component: StoreOwnerComponent },
  {path: 'affiliate-partner', component: AffiliatePartnerComponent},
  {path: 'settings', component: SettingsComponent },
  {path:'error', component: ErrorComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
