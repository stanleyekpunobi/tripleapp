import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import {AuthService} from '../shared/services/auth.service';
@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MainNavComponent implements OnInit {

  public showLoader: boolean = false;

  public isAuthenticating: boolean;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver, private authService: AuthService) {}

  ngOnInit(){
    this.authService.showloader.subscribe((response: boolean) =>{
      this.showLoader = response;
      console.log(`show loader emited ${response}`)

    })
    this.authService.isAuthenticating.subscribe((result:boolean) => {
      this.isAuthenticating = result;
    })
  }
  LogOut(){
    const storeUrl = `https://${this.authService.GetLocalStorageItem('authToken').logoutUrl}/admin/apps`;
    this.authService.RemoveToken();
    window.location.href = storeUrl;
  }
}
