import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//material angular components
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatSortModule} from '@angular/material/sort';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthComponent } from './auth/auth.component';
import { HandshakeComponent } from './auth/handshake/handshake.component';
import { HttpClientModule } from '@angular/common/http';
import { UtilService } from './shared/services/util.service';
import { ChargeComponent } from './auth/charge/charge.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatCardModule} from '@angular/material/card';
import { ProductCenterComponent } from './product-center/product-center.component';
import { StoreOwnerComponent } from './store-owner/store-owner.component';
import { AffiliatePartnerComponent } from './affiliate-partner/affiliate-partner.component';
import { SettingsComponent } from './settings/settings.component';
import { AuthService } from './shared/services/auth.service';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTableModule} from '@angular/material/table';
import {MatDialogModule} from '@angular/material/dialog';
import { ListedProductsComponent } from './store-owner/listed-products/listed-products.component';
import { ShopifyProductsComponent } from './store-owner/shopify-products/shopify-products.component';
import { StoreOwnerDialogComponent } from './store-owner/store-owner-dialog/store-owner-dialog.component';
import {MatInputModule} from '@angular/material/input';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { FormsModule } from '@angular/forms';
import { ProductCenterDialogComponent } from './product-center/product-center-dialog/product-center-dialog.component';
import { AffiliateEarningsComponent } from './affiliate-partner/affiliate-earnings/affiliate-earnings.component';
import { AffiliateProductsComponent } from './affiliate-partner/affiliate-products/affiliate-products.component';
import { ErrorComponent } from './error/error.component';
import {MatMenuModule} from '@angular/material/menu';
import {MatSelectModule} from '@angular/material/select';
import {MatPaginatorModule} from '@angular/material/paginator';
import { PricingComponent } from './settings/pricing/pricing.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatBadgeModule} from '@angular/material/badge';
import { ProductPreviewComponent } from './settings/product-preview/product-preview.component';
import { ProductRequestsComponent } from './store-owner/product-requests/product-requests.component';
import { httpInterceptorProviders } from './shared/services/interceptors.provider';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    HandshakeComponent,
    ChargeComponent,
    MainNavComponent,
    ProductCenterComponent,
    StoreOwnerComponent,
    AffiliatePartnerComponent,
    SettingsComponent,
    ListedProductsComponent,
    ShopifyProductsComponent,
    StoreOwnerDialogComponent,
    ProductCenterDialogComponent,
    AffiliateEarningsComponent,
    AffiliateProductsComponent,
    ErrorComponent,
    PricingComponent,
    ProductPreviewComponent,
    ProductRequestsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    MatSnackBarModule,
    HttpClientModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatTabsModule,
    MatTableModule,
    MatDialogModule,
    MatInputModule,
    MatProgressBarModule,
    MatMenuModule,
    MatSelectModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatSortModule,
    MatBadgeModule
  ],
  providers: [httpInterceptorProviders,UtilService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
