import { Component, OnInit } from '@angular/core';
import { ResponseObject } from '../shared/models/responseobject.model';
import { StoreOwnerStats } from '../shared/models/storeownerstats.model';
import { AuthService } from '../shared/services/auth.service';
import { StoreOwnerService } from '../shared/services/storeowner.service';

@Component({
  selector: 'app-store-owner',
  templateUrl: './store-owner.component.html',
  styleUrls: ['./store-owner.component.css'],
  providers:[StoreOwnerService]

})
export class StoreOwnerComponent implements OnInit {

  tabIndex: number = 0 ;

  public storeOwnerStats: StoreOwnerStats = new StoreOwnerStats(0,0,0);

  constructor(private storeOwnerService: StoreOwnerService, private authService: AuthService) { }

  ngOnInit(): void {

    this.storeOwnerService.GeStoreOwnerStats().subscribe((result:ResponseObject) =>{
      if(result.isSuccessful){
        //console.log(result);
        this.storeOwnerStats = result.responseData;
      }
    })


  }

  changeTab(event){
    this.tabIndex = event.index;
 }

}
