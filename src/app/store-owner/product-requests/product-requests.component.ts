import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ResponseObject } from 'src/app/shared/models/responseobject.model';
import { AuthService } from 'src/app/shared/services/auth.service';
import { StoreOwnerService } from 'src/app/shared/services/storeowner.service';
import { StoreOwnerDialogComponent } from '../store-owner-dialog/store-owner-dialog.component';

@Component({
  selector: 'app-product-requests',
  templateUrl: './product-requests.component.html',
  styleUrls: ['./product-requests.component.css']
})
export class ProductRequestsComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  shopifyProducts: any = [];

  displayedColumns: string[] = ['image', 'title', 'partnerurl', 'action'];

  dataSource = new MatTableDataSource<any>(this.shopifyProducts);

  constructor(private storeOwnerService: StoreOwnerService, public dialog: MatDialog, private authService: AuthService) { }

  ngOnInit(): void {
    this.authService.showloader.emit(true);
    this.storeOwnerService.GetListRequests().subscribe((result:ResponseObject) => {
      console.log(result);
      if(result.isSuccessful){
        this.shopifyProducts = result.responseData;
        this.dataSource = new MatTableDataSource(this.shopifyProducts);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.authService.showloader.emit(false);
      }
    })

    this.storeOwnerService.isListingApproved.subscribe((result:any)=> {
      console.log(result);
      this.shopifyProducts[result.productindex].isApproved = result?.isapproved;
      console.log(this.shopifyProducts[result.productindex]);
     });

  }

  OpenApproveDialog(product: any) {
    let dialogRef = this.dialog.open(StoreOwnerDialogComponent, {
      width:'300px',
      data: {product,isproductlisting:false, setcommission: true, isApproval:true, isRevoke: false},
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      let product = this.shopifyProducts.findIndex(e => e.affiliateProductId == result);
      console.log(product);
      this.storeOwnerService.isListingApproved.emit({productindex:product, isapproved:true });
    })
  }

  OpenRevokeDialog(product: any){
    const dialogRef = this.dialog.open(StoreOwnerDialogComponent, {
      width:'300px',
      data: {product,isproductlisting:false, setcommission: true, isApproval:true, isRevoke: true},
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      let product = this.shopifyProducts.findIndex(e => e.affiliateProductId == result);
      console.log(product);
      this.storeOwnerService.isListingApproved.emit({productindex:product, isapproved:false });
    })
  }

}
