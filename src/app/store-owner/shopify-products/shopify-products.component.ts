import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ResponseObject } from 'src/app/shared/models/responseobject.model';
import { AuthService } from 'src/app/shared/services/auth.service';
import { StoreOwnerService } from 'src/app/shared/services/storeowner.service';
import { StoreOwnerDialogComponent } from '../store-owner-dialog/store-owner-dialog.component';

@Component({
  selector: 'app-shopify-products',
  templateUrl: './shopify-products.component.html',
  styleUrls: ['./shopify-products.component.css']
})
export class ShopifyProductsComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  shopifyProducts: any = [];

  displayedColumns: string[] = ['image', 'title', 'price', 'action'];

  dataSource = new MatTableDataSource<any>(this.shopifyProducts);

  constructor(private storeOwnerService: StoreOwnerService, public dialog: MatDialog, private authService: AuthService) { }

  ngOnInit(): void {
    this.authService.showloader.emit(true);

    this.storeOwnerService.GetShopifyProducts().subscribe((result: ResponseObject) => {
      if(result.isSuccessful){
        this.shopifyProducts = result.responseData;
        this.dataSource = new MatTableDataSource(this.shopifyProducts);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        console.log(this.shopifyProducts);
        this.authService.showloader.emit(false);
      }

    })

    this.storeOwnerService.isListed.subscribe((result:any)=> {
      this.shopifyProducts[result.productindex].isListed = result.islisted;
      console.log(result);
     });
  }
  OpenListDialog(product: any) {
    let dialogRef = this.dialog.open(StoreOwnerDialogComponent, {
      width:'300px',
      data: {product, isproductlisting: true, setcommission: false},
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      let product = this.shopifyProducts.findIndex(e => e.id == result);
      console.log(product);
      this.storeOwnerService.isListed.emit({productindex:product, islisted:true });
    })
  }

  OpenUnlistDialog(product: any){
    const dialogRef = this.dialog.open(StoreOwnerDialogComponent, {
      width:'300px',
      data: {product, isproductlisting: false, setcommission: false},
    });

    dialogRef.afterClosed().subscribe(result => {
      let product = this.shopifyProducts.findIndex(e => e.id == result);
      this.storeOwnerService.isListed.emit({productindex:product, islisted:false});
    })
  }
}
