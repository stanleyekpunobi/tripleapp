import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ResponseObject } from 'src/app/shared/models/responseobject.model';
import { AuthService } from 'src/app/shared/services/auth.service';
import { StoreOwnerService } from 'src/app/shared/services/storeowner.service';
import { StoreOwnerDialogComponent } from '../store-owner-dialog/store-owner-dialog.component';

@Component({
  selector: 'app-listed-products',
  templateUrl: './listed-products.component.html',
  styleUrls: ['./listed-products.component.css']
})
export class ListedProductsComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  listedproducts: any = [];

  displayedColumns: string[] = ['image', 'title', 'price','commission', 'action'];

  dataSource = new MatTableDataSource<any>(this.listedproducts);

  constructor(private storeOwnerService: StoreOwnerService, public dialog: MatDialog, private authService: AuthService) { }

  ngOnInit(): void {
    this.authService.showloader.emit(true);

    this.storeOwnerService.GetListedProducts().subscribe((result: ResponseObject) => {
      if(result.isSuccessful){
        this.listedproducts = result.responseData;
        this.dataSource = new MatTableDataSource(this.listedproducts);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.authService.showloader.emit(false);
      }
      console.log(this.listedproducts);
    })

    this.storeOwnerService.setCommission.subscribe((result:any)=> {
      console.log(result);
      this.listedproducts[result.productindex].commission = +result.commission;
      console.log(this.listedproducts[result.productindex]);
     });
  }

  OpenListedProductSettingsDialog(product: any) {
    //console.log(product);
    let dialogRef = this.dialog.open(StoreOwnerDialogComponent, {
      width:'300px',
      data: {product, isproductlisting: false, setcommision: true, isApproval: false},
    });

    dialogRef.afterClosed().subscribe(result => {
      //console.log(result);
      let product = this.listedproducts.findIndex(e => e.id == result.productid);
      console.log(product);
      this.storeOwnerService.setCommission.emit({productindex:product, commission:result.commission});
    })
  }

}
