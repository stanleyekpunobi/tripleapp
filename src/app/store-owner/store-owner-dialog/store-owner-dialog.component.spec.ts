import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreOwnerDialogComponent } from './store-owner-dialog.component';

describe('StoreOwnerDialogComponent', () => {
  let component: StoreOwnerDialogComponent;
  let fixture: ComponentFixture<StoreOwnerDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreOwnerDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreOwnerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
