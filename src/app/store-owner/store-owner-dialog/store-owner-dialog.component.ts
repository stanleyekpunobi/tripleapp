import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ResponseObject } from 'src/app/shared/models/responseobject.model';
import { StoreOwnerService } from 'src/app/shared/services/storeowner.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-store-owner-dialog',
  templateUrl: './store-owner-dialog.component.html',
  styleUrls: ['./store-owner-dialog.component.css'],
  providers:[StoreOwnerService]
})
export class StoreOwnerDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public dialogData:any,public dialogRef: MatDialogRef<StoreOwnerDialogComponent>,
  private snackBar: MatSnackBar,
  private storeownerservice: StoreOwnerService) { }

  ngOnInit(): void {
    console.log(this.dialogData);
    //this.commission = this.dialogData.product.commission;
  }

  ListStoreOwnerProduct(){
    if(this.dialogData != null){
     this.storeownerservice.ListProduct(this.dialogData).subscribe((response:ResponseObject) => {
      if(response.isSuccessful){
        this.dialogRef.close(this.dialogData.product.id);
        Swal.fire({
          icon: 'success',
          title: 'List Product',
          text: 'Product successfully listed',
          confirmButtonColor: '#4680FF'
        })
        // this.snackBar.open("Product successfully listed", "List Product", {duration: 2000});
      }
      else{
        Swal.fire({
          icon: 'error',
          title: 'List Product',
          text: response.message,
          confirmButtonColor: '#4680FF'

        })
        //this.snackBar.open(response.message, "Invalid Product", {duration: 4000})

      }
     })
    }
    else {
      Swal.fire({
        icon: 'error',
        title: 'List Product',
        text: 'Invalid product selected',
        confirmButtonColor: '#4680FF'

      })
      //this.snackBar.open("Invalid product selected", "Invalid Product", {duration: 2000})
    }
  }

  UnListStoreOwnerProduct(){
    if(this.dialogData != null){
      this.storeownerservice.UnListProduct(this.dialogData).subscribe((response:ResponseObject) => {
        if(response.isSuccessful){
          this.dialogRef.close(this.dialogData.product.id);
          Swal.fire({
            icon: 'success',
            title: 'List Product',
            text: 'Product successfully Unlisted',
            confirmButtonColor: '#4680FF'
          })
         //this.snackBar.open("Product successfully unlisted", "List Product", {duration: 2000})

        }
        else{
          Swal.fire({
            icon: 'error',
            title: 'Unlist Product',
            text: response.message,
            confirmButtonColor: '#4680FF'

          })

        }
      })
     }
     else {
      Swal.fire({
        icon: 'error',
        title: 'Unlist Product',
        text: 'Invalid product selected',
        confirmButtonColor: '#4680FF'

      })
       //this.snackBar.open("Invalid product selected", "Invalid Product", {duration: 2000})
     }
  }

  SetProductCommision(amount: number){
    if(this.dialogData != null){
      this.dialogData.product.commission = amount;
      console.log(this.dialogData);
      this.storeownerservice.SetCommission(this.dialogData).subscribe((response:ResponseObject) => {
        if(response.isSuccessful){
          this.dialogRef.close({productid:this.dialogData.product.id, commission: this.dialogData.product.commission});
         //this.snackBar.open("Product commission saved", "Product Commission", {duration: 2000})
         Swal.fire({
          icon: 'success',
          title: 'Product Commission',
          text: 'Product commission saved',
          confirmButtonColor: '#4680FF'
        })
        }else{
          Swal.fire({
            icon: 'error',
            title: 'Product Commission',
            text: response.message,
            confirmButtonColor: '#4680FF'

          })
          //this.snackBar.open(response.message, "Invalid Product", {duration: 4000})

        }
      })
     }
     else {
      Swal.fire({
        icon: 'error',
        title: 'Product Commission',
        text: 'Invalid product selected',
        confirmButtonColor: '#4680FF'

      })
       //this.snackBar.open("Invalid product selected", "Invalid Product", {duration: 2000})
     }
  }

  ApprovePartnerListing(){
    if(this.dialogData != null){
      this.storeownerservice.ApproveListRequest(this.dialogData).subscribe((response:ResponseObject) => {
        if(response.isSuccessful){
          this.dialogRef.close(this.dialogData.product.affiliateProductId);
          Swal.fire({
            icon: 'success',
            title: 'Product List Request',
            text: 'Product listing request approved',
            confirmButtonColor: '#4680FF'
          })
         //this.snackBar.open("Product listing request approved", "Product List Request", {duration: 2000})

        }else{
          Swal.fire({
            icon: 'error',
            title: 'Product List Request',
            text: response.message,
            confirmButtonColor: '#4680FF'

          })
        }
      })
     }
     else {
      Swal.fire({
        icon: 'error',
        title: 'Invalid Product',
        text: 'Invalid product selected',
        confirmButtonColor: '#4680FF'

      })
       //this.snackBar.open("Invalid product selected", "Invalid Product", {duration: 2000})
     }
  }

  RevokePartnerListing(){
    if(this.dialogData != null){
      this.storeownerservice.RevokeListRequest(this.dialogData).subscribe((response:ResponseObject) => {
        if(response.isSuccessful){
          this.dialogRef.close(this.dialogData.product.affiliateProductId);
         //this.snackBar.open("Product listing request revoked", "Product List Request", {duration: 2000})
         Swal.fire({
          icon: 'success',
          title: 'Product List Request',
          text: 'Product listing request revoked',
          confirmButtonColor: '#4680FF'
        })
        }else{
          Swal.fire({
            icon: 'error',
            title: 'Product List Request',
            text: response.message,
            confirmButtonColor: '#4680FF'

          })
        }
      })
     }
     else {
      Swal.fire({
        icon: 'error',
        title: 'Invalid Product',
        text: 'Invalid product selected',
        confirmButtonColor: '#4680FF'

      })
       //this.snackBar.open("Invalid product selected", "Invalid Product", {duration: 2000})
     }
  }
}
