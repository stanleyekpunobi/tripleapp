import { Component, OnInit } from '@angular/core';
import { ResponseObject } from 'src/app/shared/models/responseobject.model';
import { SettingsService } from 'src/app/shared/services/settings.service';

@Component({
  selector: 'app-product-preview',
  templateUrl: './product-preview.component.html',
  styleUrls: ['./product-preview.component.css'],
  providers:[SettingsService]

})
export class ProductPreviewComponent implements OnInit {

  constructor(private settingsService: SettingsService) { }

  public isLoading: boolean = true;

  public listedProducts: any;

  ngOnInit(): void {
    this.settingsService.GetProductPreview().subscribe((result:ResponseObject) => {
      if(result.isSuccessful){
        this.isLoading = false;
        this.listedProducts = result.responseData;
      }
      console.log(result);
    })
  }

}
