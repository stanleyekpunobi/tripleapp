import { Component, OnInit } from '@angular/core';
import { ResponseObject } from 'src/app/shared/models/responseobject.model';
import { AuthService } from 'src/app/shared/services/auth.service';
import { SettingsService } from 'src/app/shared/services/settings.service';

@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.css']
})
export class PricingComponent implements OnInit {
  checked = true;
  public userSubscription: any;
  constructor(private settingsService: SettingsService, private authService: AuthService) { }
  ngOnInit(): void {
    this.authService.showloader.emit(true);
    this.settingsService.GetUserCurrentSubscription().subscribe((result:ResponseObject) => {
      if(result.isSuccessful){
        this.userSubscription = result.responseData;
        console.log(result.responseData)
      }
      this.authService.showloader.emit(false);
    })
  }

  ActivatePlan(planid: number){
    console.log(planid);
    this.authService.showloader.emit(true);
    this.authService.AuthCharge('', planid).subscribe((chargeresult: ResponseObject) => {
      if(chargeresult.isSuccessful){
        //debugger;
        window.location.href = chargeresult.responseData.url;
      }
      this.authService.showloader.emit(false);
    })
  }

}
