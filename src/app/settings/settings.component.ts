import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ResponseObject } from '../shared/models/responseobject.model';
import { SettingsModel } from '../shared/models/settings.model';
import { AuthService } from '../shared/services/auth.service';
import { SettingsService } from '../shared/services/settings.service';
import { ProductPreviewComponent } from './product-preview/product-preview.component';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css'],
  providers:[SettingsService]
})
export class SettingsComponent implements OnInit {

  public settingsModel : SettingsModel = new SettingsModel();

  constructor(private settingsService: SettingsService, private authService: AuthService,
     private snackBar: MatSnackBar, public dialog: MatDialog) { }

  ngOnInit(): void {
     this.GetUserSettings();
  }

  GetUserSettings(){
    this.authService.showloader.emit(true);
    this.settingsService.GetUserSettings().subscribe((result:ResponseObject) => {
      if(result.isSuccessful){
        console.log(result);
        if(result.responseData == null){

        }else{
          this.settingsModel.AffiliateHeading = result.responseData.affiliateHeading;
          this.settingsModel.AffiliateTag = result.responseData.affiliateTag;
          this.settingsModel.ButtonText = result.responseData.buttonText;
          this.settingsModel.PaypalEmail = result.responseData.paypalEmail;
        }
        console.log(result);
        this.authService.showloader.emit(false);

      }
    })
  }

  UpdateSettings(form: NgForm){
    if(form.valid){
      this.authService.showloader.emit(true);
      this.settingsService.UpdateUserSettings(this.settingsModel).subscribe((result:ResponseObject) =>{
        if(result.isSuccessful){
          this.authService.showloader.emit(false);
          this.snackBar.open(result.message, "Update Settings", {duration: 2000})
          this.GetUserSettings();
        }
      })
    }
  }

  openPreviewDialog(): void {
    const dialogRef = this.dialog.open(ProductPreviewComponent, {
      width: '70%',
      height:'400px'
    });
  }

}


