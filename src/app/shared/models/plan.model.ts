export class PlanModel{
  public PlanId: number;
  public Amount: number;
  public Description: string;
  public Name: string;
}
