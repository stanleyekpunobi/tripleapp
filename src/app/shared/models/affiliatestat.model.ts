export class AffiliateStatModel{
  public totalProductAdded: number;
  public totalPaid: number;
  public totalUnPaid: number;
  public topSeller: string;

  constructor(added: number, paid:number, unpaid:number, topseller: string) {
   this.totalProductAdded = added;
   this.totalPaid = paid;
   this.totalUnPaid = unpaid;
   this.topSeller = topseller;
  }
}
