export class StoreOwnerStats{
  public totalSales: number;
  public totalCommissionPaidOut: number;
  public totalListed: number;

  constructor(sales: number, commission:number, listed: 0) {
   this.totalCommissionPaidOut = commission;
   this.totalListed = listed;
   this.totalSales = sales;

  }
}
