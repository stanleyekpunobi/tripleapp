export interface ResponseObject{
  isSuccessful: boolean,
  message: string,
  responseData: any,
  urlRoute: string
}
