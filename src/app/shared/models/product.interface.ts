export interface product{
  title: string;
  id: number;
  images: images [];
  isAdded: boolean;
  shopOwner: string;
  commission: number;
  currency: string;
  variants: variants[]
  shopDomain:string;
  handle: string;
}
export interface images{
  src: string;
}
export interface variants{
  price: number;
}
