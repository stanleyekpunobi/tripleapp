export class SettingsModel{
  public DisplayOnSearch : boolean;
  public ButtonText: string;
  public AffiliateHeading: string;
  public PaypalEmail: string;
  public AffiliateTag: string;
}
