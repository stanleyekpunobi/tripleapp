import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { catchError } from 'rxjs/operators';
import { ResponseObject } from '../models/responseobject.model';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { EventEmitter, Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { SettingsModel } from '../models/settings.model';

@Injectable()
export class SettingsService{

  private apiUrl: string = environment.apiUrl;

  private authToken: any;

  public isListed = new EventEmitter<{productindex: number, islisted: boolean}>();

  public setCommission = new EventEmitter<{productindex: number, commission: number}>();


  constructor(private httpClient: HttpClient, private authService: AuthService) {
    this.authToken = authService.GetLocalStorageItem("authToken").accessToken;
   console.log(this.authToken);
  }

  UpdateUserSettings(settings: SettingsModel): Observable<ResponseObject> {
    return this.httpClient.post<ResponseObject>(this.apiUrl + "api/Settings/UpdateUserSettings", settings, { headers: { 'Authorization': `Bearer ${this.authToken}` } }).pipe(
    );
  }

  GetUserSettings(){
    return this.httpClient.get<ResponseObject>(this.apiUrl + "api/Settings/GetUserSettings", { headers: { 'Authorization': `Bearer ${this.authToken}` } }).pipe(
    );
  }

  GetUserCurrentSubscription(){
    return this.httpClient.get<ResponseObject>(this.apiUrl + "api/Settings/GetUserSubscription", { headers: { 'Authorization': `Bearer ${this.authToken}` } }).pipe(
      );
  }

  GetProductPreview(){
    return this.httpClient.get<ResponseObject>(this.apiUrl + "api/AffiliatePartner/GetProductPreview", { headers: { 'Authorization': `Bearer ${this.authToken}` } }).pipe(
      );
  }

  ActivatePlan(planid: number){
    return this.httpClient.post<ResponseObject>(`${this.apiUrl}api/Settings/ActivatePlan/${planid}`, { headers: { 'Authorization': `Bearer ${this.authToken}` } }).pipe(
      );
  }

}
