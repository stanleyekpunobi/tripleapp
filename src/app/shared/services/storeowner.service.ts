import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { catchError } from 'rxjs/operators';
import { ResponseObject } from '../models/responseobject.model';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { EventEmitter, Injectable } from '@angular/core';
import { AuthService } from './auth.service';

@Injectable()
export class StoreOwnerService {

  private apiUrl: string = environment.apiUrl;

  private authToken: any;

  public isListed = new EventEmitter<{productindex: number, islisted: boolean}>();

  public isListingApproved = new EventEmitter<{productindex: number, isapproved: boolean}>();

  public setCommission = new EventEmitter<{productindex: number, commission: number}>();


  constructor(private httpClient: HttpClient, private authService: AuthService) {
    this.authToken = authService.GetLocalStorageItem("authToken").accessToken;
   console.log(this.authToken);
  }

  GetShopifyProducts(): Observable<ResponseObject> {

    return this.httpClient.post<ResponseObject>(this.apiUrl + "api/StoreOwner/GetProducts", null, { headers: { 'Authorization': `Bearer ${this.authToken}` } }).pipe(

      );
  }

  ListProduct(storeproduct: any): Observable<ResponseObject> {
    return this.httpClient.post<ResponseObject>(this.apiUrl + "api/StoreOwner/ListProduct/" + storeproduct.product.id, null, { headers: { 'Authorization': `Bearer ${this.authToken}` } }).pipe(
      );
  }

  UnListProduct(storeproduct: any): Observable<ResponseObject> {
    return this.httpClient.post<ResponseObject>(this.apiUrl + "api/StoreOwner/UnListProduct/" + storeproduct.product.id, null, { headers: { 'Authorization': `Bearer ${this.authToken}` } }).pipe(
      );
  }


  GetListedProducts(): Observable<ResponseObject> {

    return this.httpClient.post<ResponseObject>(this.apiUrl + "api/StoreOwner/GetListedProducts", null, { headers: { 'Authorization': `Bearer ${this.authToken}` } }).pipe(

      );
  }
  SetCommission(storeproduct: any): Observable<ResponseObject> {
    return this.httpClient.post<ResponseObject>(`${this.apiUrl}api/StoreOwner/SetCommission/${storeproduct.product.id}/${storeproduct.product.commission}`, null, { headers: { 'Authorization': `Bearer ${this.authToken}` } }).pipe(
      );
  }

  ApproveListRequest(product:any): Observable<ResponseObject> {
    return this.httpClient.post<ResponseObject>(`${this.apiUrl}api/StoreOwner/ApproveListRequest/${product.product.affiliateProductId}`, null, { headers: { 'Authorization': `Bearer ${this.authToken}` } }).pipe(
      );
  }
  RevokeListRequest(product:any): Observable<ResponseObject> {
    return this.httpClient.post<ResponseObject>(`${this.apiUrl}api/StoreOwner/RevokeListRequest/${product.product.affiliateProductId}`, null, { headers: { 'Authorization': `Bearer ${this.authToken}` } }).pipe(
      );
  }

  GeStoreOwnerStats(): Observable<ResponseObject> {
    return this.httpClient.post<ResponseObject>(this.apiUrl + "api/StoreOwner/GetStoreOwnerStats", null, { headers: { 'Authorization': `Bearer ${this.authToken}` } }).pipe(
      );
  }
  GetListRequests(): Observable<ResponseObject> {
    return this.httpClient.post<ResponseObject>(this.apiUrl + "api/StoreOwner/GetListRequests", null, { headers: { 'Authorization': `Bearer ${this.authToken}` } }).pipe(
      );
  }
}
