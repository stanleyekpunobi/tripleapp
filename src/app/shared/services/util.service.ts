import {MatSnackBarModule, MatSnackBar} from '@angular/material/snack-bar';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { throwError } from 'rxjs'
import { Injectable } from '@angular/core';

@Injectable()
export class UtilService {

  constructor(private httpClient: HttpClient, private snackBar: MatSnackBar) {

  }

  public handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    }
    else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
     this.snackBar.open('Something happened on our end, please try again later!')
    );
  }

}
