import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from "@angular/common/http";
import { catchError, map } from "rxjs/operators";
import { Observable, throwError } from "rxjs";
import { Injectable } from "@angular/core";
import Swal, { SweetAlertResult } from "sweetalert2";
import { AuthService } from "./auth.service";

/** Pass untouched request through to the next request handler. */
@Injectable()
export class CustomHttpInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    //this.authService.showloader.emit(true);
    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => event), // pass further respone
      catchError((error: HttpErrorResponse) => {
        // here will be catched error from response, just check if its 401
        if (error && error.status == 401) {
          Swal.fire({
            icon: "error",
            title: "Session Expired",
            text: "Your session has expired",
            confirmButtonText: `Login from store`,
          }).then((result: SweetAlertResult) => {
            const storeUrl = `https://${this.authService.GetLocalStorageItem('authToken').logoutUrl}/admin/apps`;
            window.location.href = storeUrl;
          });
          this.authService.showloader.emit(false);
          return throwError(error);
        }
      })

    );
  }
}
