import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { catchError } from 'rxjs/operators';
import { ResponseObject } from '../models/responseobject.model';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { EventEmitter, Injectable } from '@angular/core';
import { AuthService } from './auth.service';

@Injectable()
export class AffiliatePartnerService {

  private apiUrl: string = environment.apiUrl;

  private authToken: any;

  public isAdded = new EventEmitter<{productindex: number, isadded: boolean}>();

  constructor(private httpClient: HttpClient, private authService: AuthService) {
    this.authToken = authService.GetLocalStorageItem("authToken").accessToken;
   // console.log(this.authToken);
  }

  GetAffiliateProducts(): Observable<ResponseObject> {

    return this.httpClient.post<ResponseObject>(this.apiUrl + "api/AffiliatePartner/GetAffiliateProducts", null, { headers: { 'Authorization': `Bearer ${this.authToken}` } }).pipe(

      );
  }
  GetAffiliateStats(): Observable<ResponseObject> {
    return this.httpClient.post<ResponseObject>(this.apiUrl + "api/AffiliatePartner/GetAffiliateStats", null, { headers: { 'Authorization': `Bearer ${this.authToken}` } }).pipe(
      );
  }
  GetAffiliateEarnings(): Observable<ResponseObject> {
    return this.httpClient.post<ResponseObject>(this.apiUrl + "api/AffiliatePartner/GetAffiliateEarnings", null, { headers: { 'Authorization': `Bearer ${this.authToken}` } }).pipe(
      );
  }
}
