import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { catchError } from 'rxjs/operators';
import { ResponseObject } from '../models/responseobject.model';
import { Observable, throwError } from 'rxjs';
import { UtilService } from './util.service';
import { environment } from 'src/environments/environment';
import { EventEmitter, Injectable } from '@angular/core';
import { HandshakeModel } from '../models/handshake.model';
import { AuthModel } from '../models/auth.model';
import { ChargeModel } from '../models/charge.model';
import { Router } from '@angular/router';
import { PlanModel } from '../models/plan.model';

@Injectable()
export class AuthService {

  private apiUrl: string = environment.apiUrl;

  private authToken: any;

  public showloader = new EventEmitter<boolean>();

  public isAuthenticating = new EventEmitter<boolean>();


  constructor(private httpClient: HttpClient, private utilService: UtilService, private router: Router) {
    this.authToken = this.GetLocalStorageItem("authToken");
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
      this.router.navigate(['error']);
    }
    else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
        this.router.navigate(['error']);
    }
    return throwError(
    // console.log('error')
    this.router.navigate(['error'])
    );

  }

  ShopifyHandshake(requestObject: any): Observable<ResponseObject> {
//debugger;
    return this.httpClient.post<ResponseObject>(this.apiUrl + "api/Auth/ShopifyHandShake", requestObject,{ headers: { 'content-type': 'application/json'} } ).pipe(
        catchError(this.handleError)
      );
  }

  AuthHandshake(requestObject: AuthModel): Observable<ResponseObject> {

    return this.httpClient.post<ResponseObject>(this.apiUrl + "api/Auth/AuthHandshake", requestObject).pipe(
        catchError(this.handleError)
      );
  }

  AuthCharge(accessToken: string, planid: number): Observable<ResponseObject> {
    console.log(planid);
    //debugger
    if(accessToken === ""){
      accessToken = this.authToken.accessToken;
    }
    return this.httpClient.post<ResponseObject>(`${this.apiUrl}api/Auth/AuthCharge/${planid}`, null, { headers: { 'authorization': `Bearer ${accessToken}` } }).pipe(
        catchError(this.handleError)
      );
  }

  AuthChargeAffiliate(): Observable<ResponseObject> {
    return this.httpClient.post<ResponseObject>(this.apiUrl + "api/Auth/AuthChargeAffiliate", null, { headers: { 'Authorization': `Bearer ${this.authToken.accessToken}` } }).pipe(
        catchError(this.handleError)
      );
  }

  AuthChargeResult(chargemodel: ChargeModel): Observable<ResponseObject> {

    return this.httpClient.post<ResponseObject>(this.apiUrl + "api/Auth/AuthChargeResult/" + chargemodel.ChargeId, chargemodel, { headers: { 'Authorization': `Bearer ${this.authToken.accessToken}` } }).pipe(
        catchError(this.handleError)
      );
  }

  SaveToken(response: ResponseObject) {
    localStorage.setItem("authToken", JSON.stringify(response.responseData));
  }

  RemoveToken() {
    localStorage.removeItem("authToken");
    this.authToken = this.GetLocalStorageItem("authToken")
    if (this.authToken == null || this.authToken == undefined) {

    }
  }

  GetLocalStorageItem(key: string) {
    const authData = JSON.parse(localStorage.getItem(key));
    return authData;
  }

}
