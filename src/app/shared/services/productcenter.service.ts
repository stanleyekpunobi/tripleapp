import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { catchError } from 'rxjs/operators';
import { ResponseObject } from '../models/responseobject.model';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { EventEmitter, Injectable } from '@angular/core';
import { AuthService } from './auth.service';

@Injectable()
export class ProductCenterService {

  private apiUrl: string = environment.apiUrl;

  private authToken: any;

  public isAdded = new EventEmitter<{productindex: number, isadded: boolean}>();

  constructor(private httpClient: HttpClient, private authService: AuthService) {
    this.authToken = authService.GetLocalStorageItem("authToken").accessToken;
   // console.log(this.authToken);
  }

  CheckAffiliateTag(): Observable<ResponseObject> {

    return this.httpClient.post<ResponseObject>(this.apiUrl + "api/ProductCenter/CheckAffiliateTag", null, { headers: { 'Authorization': `Bearer ${this.authToken}` } }).pipe(

      );
  }

  AddProduct(storeproduct: any): Observable<ResponseObject> {
    return this.httpClient.post<ResponseObject>(this.apiUrl + "api/ProductCenter/AddProduct/" + storeproduct.id, null, { headers: { 'Authorization': `Bearer ${this.authToken}` } }).pipe(
      );
  }

  RemoveProduct(storeproduct: any): Observable<ResponseObject> {
    return this.httpClient.post<ResponseObject>(this.apiUrl + "api/ProductCenter/RemoveProduct/" + storeproduct.id, null, { headers: { 'Authorization': `Bearer ${this.authToken}` } }).pipe(
      );
  }

  GetListedProducts(): Observable<ResponseObject> {
    return this.httpClient.post<ResponseObject>(this.apiUrl + "api/ProductCenter/GetListedProducts", null, { headers: { 'Authorization': `Bearer ${this.authToken}` } }).pipe(
      );
  }
}
