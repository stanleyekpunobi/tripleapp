import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AffiliateStatModel } from '../shared/models/affiliatestat.model';
import { ResponseObject } from '../shared/models/responseobject.model';
import { AffiliatePartnerService } from '../shared/services/affiliatepartner.service';

@Component({
  selector: 'app-affiliate-partner',
  templateUrl: './affiliate-partner.component.html',
  styleUrls: ['./affiliate-partner.component.css'],
  providers: [AffiliatePartnerService]
})
export class AffiliatePartnerComponent implements OnInit {

  affiliateStat:AffiliateStatModel = new AffiliateStatModel(0,0,0,"https://via.placeholder.com/100")
  tabIndex: number = 0 ;

  constructor(private affiliatePartnerService: AffiliatePartnerService) { }

  ngOnInit(): void {
    this.affiliatePartnerService.GetAffiliateStats().subscribe((result:ResponseObject) => {
      if(result.isSuccessful){
        this.affiliateStat = result.responseData
        if(result.responseData.topSeller == null || result.responseData.topSeller == ''){
          this.affiliateStat.topSeller = 'https://via.placeholder.com/300';
        }

      }
    })
  }

  changeTab(event){
    this.tabIndex = event.index;
 }
}
