import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ProductCenterDialogComponent } from 'src/app/product-center/product-center-dialog/product-center-dialog.component';
import { ResponseObject } from 'src/app/shared/models/responseobject.model';
import { AffiliatePartnerService } from 'src/app/shared/services/affiliatepartner.service';
import {MatTableDataSource} from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { AuthService } from 'src/app/shared/services/auth.service';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-affiliate-products',
  templateUrl: './affiliate-products.component.html',
  styleUrls: ['./affiliate-products.component.css']
})
export class AffiliateProductsComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  listedProducts: any = [];

  displayedColumns: string[] = ['image', 'title', 'price','commission','status', 'action'];

  dataSource = new MatTableDataSource<any>(this.listedProducts);

  constructor(public dialog: MatDialog, private affiliatePartnerService: AffiliatePartnerService, private authService: AuthService) { }

  ngOnInit(): void {
    this.authService.showloader.emit(true);
    this.affiliatePartnerService.GetAffiliateProducts().subscribe((result: ResponseObject) => {
      if(result.isSuccessful){
        this.listedProducts = result.responseData;
        this.dataSource = new MatTableDataSource(this.listedProducts);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.authService.showloader.emit(false);

      }
      console.log(result);

    })

    this.affiliatePartnerService.isAdded.subscribe((result:any)=> {
      this.listedProducts[result.productindex].isAdded = result.isadded;
      console.log(result);
     });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  OpenRemoveDialog(product: any){
    let dialogRef = this.dialog.open(ProductCenterDialogComponent, {
      width:'300px',
      data: {product, isadding: false},
    });

    dialogRef.afterClosed().subscribe(result => {
      let product = this.listedProducts.findIndex(e => e.id == result);
      this.affiliatePartnerService.isAdded.emit({productindex:product, isadded:false });
    })
  }

  OpenAddDialog(product: any){
    let dialogRef = this.dialog.open(ProductCenterDialogComponent, {
      width:'300px',
      data: {product, isadding: true},
    });

    dialogRef.afterClosed().subscribe(result => {
      let product = this.listedProducts.findIndex(e => e.id == result);
      this.affiliatePartnerService.isAdded.emit({productindex:product, isadded:true });
    })
  }
}
