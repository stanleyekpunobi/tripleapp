import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ResponseObject } from 'src/app/shared/models/responseobject.model';
import { AffiliatePartnerService } from 'src/app/shared/services/affiliatepartner.service';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-affiliate-earnings',
  templateUrl: './affiliate-earnings.component.html',
  styleUrls: ['./affiliate-earnings.component.css']
})
export class AffiliateEarningsComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  earnings: any = [];

  displayedColumns: string[] = ['date','orderid','quantity', 'saleamount','commission', 'status'];

  dataSource = new MatTableDataSource<any>(this.earnings);

  constructor(public dialog: MatDialog, private affiliatePartnerService: AffiliatePartnerService, private authService: AuthService) { }


  ngOnInit(): void {
    this.authService.showloader.emit(true);
    this.affiliatePartnerService.GetAffiliateEarnings().subscribe((result:ResponseObject) =>{
      if(result.isSuccessful){
        this.earnings = result.responseData;
        this.dataSource = new MatTableDataSource(this.earnings);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.authService.showloader.emit(false);
        console.log(result.responseData)
      }
    });

  }

}
