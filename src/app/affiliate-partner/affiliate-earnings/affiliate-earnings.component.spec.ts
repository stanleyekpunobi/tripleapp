import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffiliateEarningsComponent } from './affiliate-earnings.component';

describe('AffiliateEarningsComponent', () => {
  let component: AffiliateEarningsComponent;
  let fixture: ComponentFixture<AffiliateEarningsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffiliateEarningsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffiliateEarningsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
