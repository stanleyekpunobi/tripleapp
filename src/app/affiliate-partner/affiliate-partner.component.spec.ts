import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffiliatePartnerComponent } from './affiliate-partner.component';

describe('AffiliatePartnerComponent', () => {
  let component: AffiliatePartnerComponent;
  let fixture: ComponentFixture<AffiliatePartnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffiliatePartnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffiliatePartnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
